﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using TPUExamLib;
namespace TPUExamOffline
{
  /// <summary>
  /// Логика взаимодействия для wndInvite.xaml
  /// </summary>
  public partial class wndInvite : Window
  {
    bool IsReallyClose = false;

    Student _student;
    public wndInvite(Student student)
    {
      _student = student;

      InitializeComponent();
      grid_fio.DataContext = _student;
    }

    private
    void b_Next_Click(object sender, RoutedEventArgs e)
    {
      wndExam wnd = new wndExam(_student);
      wnd.CloseHandler += new wndExam.CloseEventHandler(wnd_CloseHandler);
      Hide();
      wnd.Show();
    }

    void wnd_CloseHandler(object sender)
    {
      Close();
      OnCloseEvent(new CloseEventArgs() { IsReallyClose = true });
    }

    private void b_prev_Click(object sender, RoutedEventArgs e)
    {
      IsReallyClose = true;
      Close();
      OnCloseEvent(new CloseEventArgs() { IsReallyClose = false });
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (!IsReallyClose) e.Cancel = true;
    }


    #region events
    public class CloseEventArgs : EventArgs
    {
      public bool IsReallyClose;
    }

    public delegate void CloseEventHandler(Object sender, CloseEventArgs args);
    public event CloseEventHandler CloseHandler;
    protected virtual void OnCloseEvent(CloseEventArgs args)
    {
      if (CloseHandler != null)
      {
        CloseHandler(this, args);
      }
    }

    #endregion events

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      TopPanel.HideClock(true);
    }
  }
}

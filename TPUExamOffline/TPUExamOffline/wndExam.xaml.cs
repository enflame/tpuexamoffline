﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Threading;
using TPUExamLib;
using System.IO;

using System.Diagnostics;

namespace TPUExamOffline
{
  /// <summary>
  /// Логика взаимодействия для wndExam.xaml
  /// </summary>
  public partial class wndExam : Window
  {
    bool isReallyClosing;
    WinUtils winUtils;

    QPaginator Current_QPaginator;
    QPaginatorPackage Current_QPaginatorPackage;

    public wndExam(Student student)
    {

      try
      {

        isReallyClosing = false;
        winUtils = new WinUtils();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

      try
      {
        InitializeComponent();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

      try
      {

        bool isOnlyOneInstance;
        Mutex mtx = new Mutex(true, "TPUExamOffline", out isOnlyOneInstance); // используйте имя вашего приложения
        if (!isOnlyOneInstance)
        {
          isReallyClosing = true;
          Close();
        }

      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }


      //Init
   
      foreach (Question item in student.Examen.packages[0].ticket.GetQuestions())
      {
        QPaginator b = new QPaginator();
        b.Set(item);
        b.Width = 40;
        b.Height = 30;
        b.ClickHandler += new QPaginator.ClickEventHandler(QPaginator_ClickHandler);
        wrapPanelQuestions.Children.Add(b);
      }
      for(int i=1;i< wrapPanelQuestions.Children.Count-1; i++)
      {
        QPaginator qp1 = (QPaginator)wrapPanelQuestions.Children[i-1];
        QPaginator qp2 = (QPaginator)wrapPanelQuestions.Children[i];
        QPaginator qp3 = (QPaginator)wrapPanelQuestions.Children[i+1];

        qp1.QPaginatorNext = qp2;
        qp2.QPaginatorNext = qp3;
        qp2.QPaginatorPrev = qp1;
        qp3.QPaginatorPrev = qp2;
      }

      foreach (Package item in student.Examen.packages)
      {
        QPaginatorPackage b = new QPaginatorPackage();
        b.Set(item);
        b.ClickHandler += new QPaginatorPackage.ClickEventHandler(QPaginatorPackage_ClickHandler); ;
        wrapPanelPackages.Children.Add(b);
      }

      Current_QPaginator = (QPaginator)wrapPanelQuestions.Children[0];
      Current_QPaginator.Set(true);

      webControl.Source = new Uri(Current_QPaginator._question.PathToHtml);

      Current_QPaginatorPackage = (QPaginatorPackage)wrapPanelPackages.Children[0];
      Current_QPaginatorPackage.Set(true);
      Current_QPaginatorPackage.UpdateInfo();

      PrevQuestion.Set(true);
      PrevQuestion.ClickHandler += new QPaginatorPrevNext.ClickEventHandler(PrevQuestion_ClickHandler);
      PrevQuestion.Visibility = System.Windows.Visibility.Hidden;

      NextQuestion.Set(false);
      NextQuestion.ClickHandler += new QPaginatorPrevNext.ClickEventHandler(PrevQuestion_ClickHandler);

      tb_fio.Text = student.FIO;
    }

    void PrevQuestion_ClickHandler(object sender, QPaginatorPrevNext.ClickEventArgs args)
    {
      if (args.IsLeft)
      {
        UpdateQuestion(Current_QPaginator.QPaginatorPrev);
      }
      else 
      {
        UpdateQuestion(Current_QPaginator.QPaginatorNext);
      }
    }

    void QPaginatorPackage_ClickHandler(object sender, QPaginatorPackage.ClickEventArgs args)
    {
    }

    void QPaginator_ClickHandler(object sender, QPaginator.ClickEventArgs args)
    {
      QPaginator b = (QPaginator)sender;
      UpdateQuestion(b);
    }

    void UpdateQuestion(QPaginator qp)
    {
      if (qp == null) return;

      try
      {
        object answer = webControl.ExecuteJavascriptWithResult("_callback()");
        Current_QPaginator._question.SetAnswer(answer);
        Current_QPaginator.Set(false);

        Current_QPaginator = qp;
        Current_QPaginator.Set(true);
        webControl.Source = new Uri(qp._question.PathToHtml);

        string fun = "_initialization(" + answer.ToString() + ")";
        webControl.ExecuteJavascriptWithResult(fun);

        if (Current_QPaginator.QPaginatorNext == null)
        {
          PrevQuestion.Visibility = System.Windows.Visibility.Visible;
          NextQuestion.Visibility = System.Windows.Visibility.Hidden;
        }
        if (Current_QPaginator.QPaginatorPrev == null)
        {
          PrevQuestion.Visibility = System.Windows.Visibility.Hidden;
          NextQuestion.Visibility = System.Windows.Visibility.Visible;
        }
        if ((Current_QPaginator.QPaginatorPrev != null) & (Current_QPaginator.QPaginatorNext != null))
        {
          PrevQuestion.Visibility = System.Windows.Visibility.Visible;
          NextQuestion.Visibility = System.Windows.Visibility.Visible;
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show("UpdateQuestion: " + exc.Message);
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {

      this.Topmost = true;
      this.WindowState = System.Windows.WindowState.Maximized;
      winUtils.StartLockWindow();
      try
      {
        winUtils.StartLockKeys();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

      //таймер для постоянной активации окна
      System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();
      Timer.Tick += new EventHandler(Timer_Tick);
      Timer.Interval = new TimeSpan(0, 0, 0, 0, 300);
      Timer.Start();      
    }
    private void Timer_Tick(object sender, EventArgs e)
    {
      //активируем окно дабы скрыть кнопку пуск
      this.Activate();
      Process[] prs = Process.GetProcesses();
      foreach (Process pr in prs)
      {
        if (pr.ProcessName == "taskmgr")
          pr.Kill();
      }
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (isReallyClosing)
      {
        try
        {
          winUtils.ShowStartMenu();
          winUtils.EndLockKeys();
          //      winUtils.EnableTaskManager();
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
        }
      }
      else
      {

        e.Cancel = true;
      }

    }
    private void b_Close_Click(object sender, RoutedEventArgs e)
    {
      isReallyClosing = true;
      Close();
      OnCloseEvent();
    }

    private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.LeftCtrl)
      {
        e.Handled = false;
      }
    }

    #region events
    public delegate void CloseEventHandler(Object sender);
    public event CloseEventHandler CloseHandler;
    protected virtual void OnCloseEvent()
    {
      if (CloseHandler != null)
      {
        CloseHandler(this);
      }
    }

    #endregion events

  }
}

function init_match_question()
{
    $ = jQuery;

    $('.match_drag').each(function() {
        $(this).css({
            position: 'absolute'
        });
    });

    $('.match_drag').drag("start", function(ev, dd) {
        $(dd.available).addClass("available");
        dd.update();
        return $(this).clone()
                .css("opacity", .5)
                .addClass('cloned realtime')
                .appendTo(document.body);

    }).drag(function(ev, dd) {
        $(dd.proxy).css({
            position: 'absolute',
            top: dd.offsetY,
            left: dd.offsetX
        });

    }).drag("end", function(ev, dd) {

        if (undefined === $(dd.proxy).attr('data-droped')) {

            $(dd.proxy).animate({
                top: dd.originalY,
                left: dd.originalX
            }, 420, function() {
                $(dd.proxy).remove();
            });
        }
        else {

            var m_id = $(dd.proxy).attr('data-match');
            var label = $(dd.proxy).attr('data-label');
            var a_id = $(dd.proxy).attr('data-value');
            var m_text = $('#value_' + m_id).attr('data-label');

            $('#td_' + m_id).text(label);
            $('#hidden-value_' + m_id).val(a_id);

            $(dd.proxy).animate({
                opacity: 1,
            }, 420).text(m_text + ' : ' + label);
        }

        $(dd.available).removeClass("available");
    });

    $('.match_drop').drop(function(ev, dd) {

        $(dd.proxy).attr('data-droped', 1);
        $(dd.proxy).attr('data-match', $(this).attr('data-value'));

        var offset = $(this).offset();
        $(this).text('');

        $(dd.proxy).animate({
            top: offset.top + ($(this).height() - $(dd.proxy).height()) / 2,
            left: offset.left + ($(this).width() - $(dd.proxy).width()) / 2
        }, 420);
    });

    $('.btn-revert').on('click', function(e) {

        e.preventDefault();

        $('.answer_tr > td').html('&nbsp;');
        $('div.cloned').remove();
        $('.hidden-value').val('');

        $('.match_drop').each(function() {
            $(this).text($(this).attr('data-label'));
        });

        return false;
    });
}

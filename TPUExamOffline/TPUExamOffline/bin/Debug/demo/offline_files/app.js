/**
 Core script to handle the entire theme and core functions
 **/
var App = function () {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var layoutImgPath = 'https://storage.tpu.ru/common/img/';

    var resizeHandlers = [];

    // initializes main settings
    var handleInit = function () {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            $('html').addClass('ie10'); // detect IE10 version
        }

        if (isIE10 || isIE9 || isIE8) {
            $('html').addClass('ie'); // detect IE10 version
        }

        $('.popover-view').popover();
    };

    // runs callback functions set by Metronic.addResponsiveHandler().
    var _runResizeHandlers = function () {
        // reinitialize other subscribed elements
        for (var i = 0; i < resizeHandlers.length; i++) {
            var each = resizeHandlers[i];
            each.call();
        }
    };

    // handle the layout reinitialization on window resize
    var handleOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    _runResizeHandlers();
                }, 50); // wait 50ms until window resize finishes.
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    _runResizeHandlers();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    };

    /**
     * Handles scrollable contents using jQuery SlimScroll plugin.
     */
    var handleScrollers = function ($dom) {
        App.initSlimScroll('.scroller-div', $dom);
    };

    /**
     * Handles Bootstrap Modals.
     */
    var handleModals = function () {
        // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class.
        $('body').on('hide.bs.modal', function () {
            if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') === false) {
                $('html').addClass('modal-open');
            } else if ($('.modal:visible').size() <= 1) {
                $('html').removeClass('modal-open');
            }
        });

        // fix page scrollbars issue
        $('body').on('show.bs.modal', '.modal', function () {
            if ($(this).hasClass("modal-scroll")) {
                $('body').addClass("modal-open-noscroll");
            }
        });

        // fix page scrollbars issue
        $('body').on('hide.bs.modal', '.modal', function () {
            $('body').removeClass("modal-open-noscroll");
        });

        // remove ajax content and remove cache on modal closed
        $('body').on('hidden.bs.modal', '.modal:not(.modal-cached)', function () {
            $(this).removeData('bs.modal');
        });
    };

    /**
     * Общие параметры
     */
    var handleCommon = function () {

        $('.navigation').find('li.active').parents('li').addClass('active');
        $('.navigation').find('li').not('.active').has('ul').children('ul').addClass('hidden-ul');
        $('.navigation').find('li').has('ul').children('a').parent('li').addClass('has-ul');

        $('.navigation').find('li').has('ul').children('a').on('click', function (e) {
            e.preventDefault();

            if ($('body').hasClass('sidebar-narrow')) {
                $(this).parent('li > ul li').not('.disabled').toggleClass('active').children('ul').slideToggle(250);
                $(this).parent('li > ul li').not('.disabled').siblings().removeClass('active').children('ul').slideUp(250);
            }

            else {
                $(this).parent('li').not('.disabled').toggleClass('active').children('ul').slideToggle(250);
                $(this).parent('li').not('.disabled').siblings().removeClass('active').children('ul').slideUp(250);
            }
        });

        /* Closing */
        $('[data-panel=close]').click(function (e) {
            e.preventDefault();
            var $panelContent = $(this).parent().parent().parent();
            $panelContent.slideUp(200).remove(200);
        });

        $(document).delegate('.panel-trigger', 'click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
        });
    };

    /**
     * Fix input placeholder issue for IE8 and IE9
     */
    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            if (jQuery.placeholder) {
                $('input[placeholder]:not(input[type="password"]), textarea[placeholder]').placeholder();
            }
        }
    };

    /**
     * Handles custom checkboxes & radios using jQuery Uniform plugin
     */
    var handleUniform = function ($dom) {
        if (!jQuery().uniform) {
            return;
        }
        var test = $('input[type=checkbox]:not(.toggle, .make-switch), input[type=radio]:not(.toggle, .star, .make-switch)', $dom);
        if (test.size() > 0) {
            test.each(function () {
                if ($(this).parents(".checker").size() == 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    };

    /**
     * handle group element heights
     */
    var handleHeight = function () {
        $('[data-auto-height]').each(function () {
            var parent = $(this);
            var items = $('[data-height]', parent);
            var height = 0;
            var mode = parent.attr('data-mode');
            var offset = parseInt(parent.attr('data-offset') ? parent.attr('data-offset') : 0);

            items.each(function () {
                if ($(this).attr('data-height') == "height") {
                    $(this).css('height', '');
                } else {
                    $(this).css('min-height', '');
                }

                var height_ = (mode == 'base-height' ? $(this).outerHeight() : $(this).outerHeight(true));
                if (height_ > height) {
                    height = height_;
                }
            });

            height = height + offset;

            items.each(function () {
                if ($(this).attr('data-height') == "height") {
                    $(this).css('height', height);
                } else {
                    $(this).css('min-height', height);
                }
            });
        });
    }

    //* END:CORE HANDLERS *//

    return {

        //main function to initiate the theme
        init: function () {
            handleInit();
            handleModals();
            handleCommon();
            handleScrollers();

            //Handle group element heights
            handleHeight();
            // handle auto calculating height on window resize
            this.addResizeHandler(handleHeight);

            handleFixInputPlaceholderForIE();
            handleUniform();
        },

        //main function to initiate core javascript after ajax complete
        initAjax: function ($dom) {

            $.init_ajax_select2($dom);
            $.init_ajax_file_upload($dom);
            $.init_ajax_switch($dom);
            $.init_ajax_datetimepicker($dom);
            $.init_ajax_spinner($dom);

            if (jQuery().init_ajax_tree_select) {
                $.init_ajax_tree_select($dom);
            }
            if (jQuery().init_ajax_slider) {
                $.init_ajax_slider($dom);
            }

            $(".styled, .multiselect-container input", $dom).uniform({
                radioClass: 'choice',
                selectAutoWidth: false
            });

            handleUniform($dom);
            handleScrollers($dom);
        },

        //public function to fix the sidebar and content height accordingly
        fixContentHeight: function () {

        },

        //public function to remember last opened popover that needs to be closed on click
        setLastPopedPopover: function (el) {
            lastPopedPopover = el;
        },

        //public function to add callback a function which will be called on window resize
        addResponsiveHandler: function (func) {

        },

        // useful function to make equal height for contacts stand side by side
        setEqualHeight: function (els) {
            var tallestEl = 0;
            els = jQuery(els);
            els.each(function () {
                var currentHeight = $(this).height();
                if (currentHeight > tallestEl) {
                    tallestColumn = currentHeight;
                }
            });
            els.height(tallestEl);
        },

        // wrapper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.header').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }
            jQuery('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        /**
         * function to scroll to the top
         */
        scrollTop: function () {
            App.scrollTo();
        },

        /**
         * Инициализация
         * @param el
         */
        initSlimScroll: function (el, $dom) {

            App.destroySlimScroll(el, $dom);

            $(el, $dom).each(function () {
                if ($(this).attr("data-initialized")) {
                    return; // exit
                }

                var height;

                if ($(this).attr("data-height")) {
                    height = $(this).attr("data-height");
                } else {
                    height = $(this).css('height');
                }

                $(this).slimScroll({
                    allowPageScroll: true, // allow page scroll when the element scroll is ended
                    size: '7px',
                    color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
                    wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                    railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
                    position: isRTL ? 'left' : 'right',
                    height: height,
                    alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                    railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                    disableFadeOut: true
                });

                $(this).attr("data-initialized", "1");
            });
        },

        /**
         * отключение
         * @param el
         */
        destroySlimScroll: function (el, $dom) {
            $(el, $dom).each(function () {
                if ($(this).attr("data-initialized") === "1") { // destroy existing instance before updating the height
                    $(this).removeAttr("data-initialized");
                    $(this).removeAttr("style");

                    var attrList = {};

                    // store the custom attribures so later we will reassign.
                    if ($(this).attr("data-handle-color")) {
                        attrList["data-handle-color"] = $(this).attr("data-handle-color");
                    }
                    if ($(this).attr("data-wrapper-class")) {
                        attrList["data-wrapper-class"] = $(this).attr("data-wrapper-class");
                    }
                    if ($(this).attr("data-rail-color")) {
                        attrList["data-rail-color"] = $(this).attr("data-rail-color");
                    }
                    if ($(this).attr("data-always-visible")) {
                        attrList["data-always-visible"] = $(this).attr("data-always-visible");
                    }
                    if ($(this).attr("data-rail-visible")) {
                        attrList["data-rail-visible"] = $(this).attr("data-rail-visible");
                    }

                    $(this).slimScroll({
                        wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                        destroy: true
                    });

                    var the = $(this);

                    // reassign custom attributes
                    $.each(attrList, function (key, value) {
                        the.attr(key, value);
                    });
                }
            });
        },

        /**
         * отправить запрос на сервер
         *
         * @param ui
         * @param url
         * @param data
         * @param callback
         */
        post: function (ui, url, data, callback) {

            //App.startPageLoading();

            $.post(url, data, function (response) {
                if (callback) {
                    callback(response);
                }
                App.stopPageLoading();
            }, 'json')
                .fail(function () {
                    alert("error");
                    App.stopPageLoading();
                });
        },

        // wrapper function to  block element(indicate loading)
        blockUI: function (options) {
            var options = $.extend(true, {}, options);
            var html = '';
            if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="' + this.getImgPath() + 'loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            } else if (options.emptyText) {
                html = '';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="' + this.getImgPath() + 'loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            }

            if (options.target) { // element blocking
                var el = jQuery(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY != undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (target) {
            if (target) {
                jQuery(target).unblock({
                    onUnblock: function () {
                        jQuery(target)
                            .css('position', '')
                            .css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        /**
         * Начинаем загружать контент
         * @param message
         */
        startPageLoading: function (message) {
            $('.page-loading').remove();
            $('body').append('<div class="page-loading"><img src="' + this.getImgPath() + 'loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (message ? message : 'Loading...') + '</span></div>');
        },

        /**
         * Остановка загрузки контента
         */
        stopPageLoading: function () {
            $('.page-loading').remove();
        },

        // initializes uniform elements
        initUniform: function (els) {
            if (els) {
                jQuery(els).each(function () {
                    if ($(this).parents(".checker").size() == 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            } else {
                handleUniform();
            }
        },

        /**
         * wrapper function to update/sync jquery uniform checkbox & radios
         */
        updateUniform: function (els) {
            // update the uniform checkbox & radios UI after the actual input control state changed
            $.uniform.update(els);
        },

        /**
         * public helper function to get actual input value(used in IE9 and IE8 due to placeholder attribute not supported)
         */
        getActualVal: function (el) {
            var el = jQuery(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        /**
         * public function to get a paremeter by name from URL
         */
        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        stripURLParametr: function (paramName, url) {
            url = url ? url : window.location.href;
            return url
                .replace(/\?page=[^&]+&?/, '?')
                .replace(/page=[^&]+&?/, '')
                .replace(/[\?&]$/, '')
        },

        /**
         * get hash of url
         */
        getURLHash: function () {
            return window.location.hash.substring(1);
        },

        /**
         * перезагрузить страницу
         */
        reload: function (timeout) {

            if (timeout !== undefined) {
                setTimeout(function () {
                    window.location.reload(false);
                }, timeout);
            }
            else {
                window.location.reload(false);
            }
        },

        /**
         * check for device touch support
         */
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        getUniqueID: function (prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        notify: function (options) {

            options = $.extend(true, {
                theme: 'success',  // alert's type
                message: '',  // alert's message
                focus: true, // auto scroll to the alert after shown
                life: 0, // auto close after defined seconds
            }, options);

            $.jGrowl(options.message, {
                life: options.life,
                theme: 'growl-' + options.theme,
                header: options.header,
                closerTemplate: '<div>[ Закрыть все ]</div>'
            });
        },

        /**
         * check IE8 mode
         */
        isIE8: function () {
            return isIE8;
        },

        /**
         * check IE9 mode
         */
        isIE9: function () {
            return isIE9;
        },

        /**
         * check RTL mode
         */
        isRTL: function () {
            return isRTL;
        },

        /**
         * получить время клиента
         */
        time_zone: function () {
            var now = new Date();
            return -now.getTimezoneOffset() / 60;
        },

        /**
         * получить полное доменное имя
         */
        domain: function () {
            return window.location.protocol + '//' + window.location.host + '/';
        },

        /**
         * redirect to url
         */
        redirect: function (url) {
            window.location = url;
        },

        /**
         * set new title
         */
        title: function (value) {
            document.title = value;
        },

        /**
         * загрузить картинку
         */
        load_image: function (selector, src, callback) {
            var data = {
                'rand': Math.round(Math.random() * 1000000)
            };

            var img = $('<img id="image-target" />');
            var parent = $(selector);
            parent.empty().append(img);
            img.load(function () {
                if (callback)
                    callback(img);
            }).attr('src', src + '?' + $.param(data));
        },

        /**
         * получить информацию об IP
         */
        ipinfo: function (ip, callback) {
            if (ip) {
                $.getJSON("https://www.telize.com/geoip/" + ip + "?callback=?",
                    function (json) {
                        if (callback) {
                            callback(json)
                        }
                    }
                );
            }
        },

        /**
         * Сформировать URL
         */
        make_url: function (parent, ajax_url) {
            var parent_data = [];
            if (parent) {
                var ids = parent.split(",");
                if (ids.length > 1) {
                    $(ids).each(function (ind) {
                        var p = $('#' + ids[ind]);
                        var name = p.attr('data-name') ? p.attr('data-name') : p.attr('name');
                        parent_data.push({name: name, value: p.val()});
                    });
                }
                else {
                    var p = $('#' + parent);
                    var name = p.attr('data-name') ? p.attr('data-name') : p.attr('name');
                    parent_data.push({name: name, value: p.val()});
                }
            }

            var url = ajax_url;

            if (parent_data) {
                return url + ( strpos(url, '?') ? '&' : '?' ) + $.param(parent_data);
            }
            else {
                return url;
            }
        },

        /**
         * получить время на сервере
         */
        serverTime: function () {
            var time = null;
            $.ajax({
                url: '/server/time',
                async: false,
                dataType: 'text',
                success: function (text) {
                    time = new Date(text);
                },
                error: function (http, message, exc) {
                    time = new Date();
                }
            });
            return time;
        },

        getImgPath: function () {
            return layoutImgPath;
        },

        /**
         * public function to add callback a function which will be called on window resize
         * @param func
         */
        addResizeHandler: function (func) {
            resizeHandlers.push(func);
        },

        /**
         * public functon to call _runresizeHandlers
         */
        runResizeHandlers: function () {
            _runResizeHandlers();
        },

        /**
         * активировать - деактивировать компонент
         * @param $selector
         * @param isEnabled
         */
        enabled: function ($selector, isEnabled) {
            if (isEnabled) {
                $selector
                    .prop('disabled', '')
                    .removeClass('disabled');
            }
            else {
                $selector
                    .prop('disabled', 'disabled')
                    .addClass('disabled');
            }
        },

        /**
         * отладка
         */
        dump: function () {
            var msg = '[jquery] ' + Array.prototype.join.call(arguments, '');
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
            else if (window.opera && window.opera.postError) {
                window.opera.postError(msg);
            }
        }
    };
}();

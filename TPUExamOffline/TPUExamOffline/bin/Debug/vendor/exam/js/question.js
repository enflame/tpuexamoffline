jQuery(document).ready(function ($) {

    $('.panel_drag').drag(function (ev, dd) {
        $(this).css({
            top: dd.offsetY,
            left: dd.offsetX
        });
    }, {handle: ".handle"});


    var items_fade = $('.fade-obj');
    var items_hide = $('.hide-obj');
    $('input.choice').on('click', function (e) {
        items_fade.removeClass('in');
        items_hide.removeClass('in').addClass('hide');

        var id = $(this).attr('id');
        $('#' + id + '_icon')
            .addClass('in');

        $('#' + id + '_btn, #' + id + '_info')
            .removeClass('hide')
            .addClass('in');
    });

    function init() {
        $('.question_type').each(function () {
            var type = $(this).val();
            if (type == 'sequence') {
                init_sequence_question();
            }
            if (type == 'match') {
                init_match_question();
            }
        })
    }

    init();

    function update_progress(response) {
        var v = '#pos-' + response.pos;
        $(v)
            .removeClass('attempt attempt50')
            .addClass(response.class);

        var v = '#pos-' + response.pos + '-' + response.question;
        $(v)
            .removeClass('attempt warning danger')
            .addClass(response.class2);
    }

    if (jQuery.init_callback_e) {
        $.init_callback_e('choice', function () {

            return true;
        }, function (response) {
            update_progress(response);
            return true;
        });
    }

    if (jQuery.editable) {
        $('.inline-text').editable({
            inputclass: 'form-control input-small',
            showbuttons: false,
            success: function (response, newValue) {
                update_progress(response);
            }
        });
    }
});

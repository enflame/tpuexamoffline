function init_sequence_question() {
    $ = jQuery;

    var adjustment;
    var old_index = -1;

    $(".sequence").sortable({
        group: 'simple_with_animation',
        pullPlaceholder: false,
        // animation on drop
        onDrop: function (item, targetContainer, _super) {
            var clonedItem = $('<li/>').css({height: 0});
            item.before(clonedItem);
            clonedItem.animate({height: item.height()});

            item.animate(clonedItem.position(), function () {
                clonedItem.detach();
                _super(item);

                if (old_index !== item.index()) {
                    var f = true;
                    $('.sequence > li.value').each(function () {
                        var $i = $(this);
                        if ($i.index() == 0) {
                            if (f == false) {
                                return false;
                            }
                        }
                        if (f) {
                            f = false;
                        }
                        $('#td_' + $i.index()).text($i.attr('data-number'));
                        $('#value_' + $i.attr('data-value')).val($i.index() + 1);
                    });
                }
            });
        },
        // set item relative to cursor position
        onDragStart: function ($item, container, _super) {
            var offset = $item.offset(),
                pointer = container.rootGroup.pointer;

            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            };

            old_index = $item.index();
            _super($item, container);
        },
        onDrag: function ($item, position) {
            $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });
        }
    });
}
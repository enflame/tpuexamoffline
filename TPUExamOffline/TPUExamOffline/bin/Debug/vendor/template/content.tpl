<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Dmitry E. Semenov">
    <link rel="shortcut icon" href="/vendor/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="/vendor/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link href="/vendor/exam/css/londinium-theme.css" rel="stylesheet">
    <link href="/vendor/exam/css/styles.css" rel="stylesheet">
    <link href="/vendor/exam/css/tpu.css" rel="stylesheet">
    <link href="/vendor/icons/icons.css" rel="stylesheet">
</head>

<body>

<div class="page-container container">
    {{BODY}}
</div>

<script type="text/javascript" src="/vendor/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/vendor/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/vendor/exam/js/common.js"></script>
<script type="text/javascript" src="/vendor/exam/js/app.js"></script>

<script type="text/javascript" src="/vendor/drag-drop/drag/jquery.event.drag-2.2.js"></script>
<script type="text/javascript" src="/vendor/drag-drop/drag/jquery.event.drag.live-2.2.js"></script>
<script type="text/javascript" src="/vendor/drag-drop/drop/jquery.event.drop-2.2.js"></script>
<script type="text/javascript" src="/vendor/drag-drop/drop/jquery.event.drop.live-2.2.js"></script>
<script type="text/javascript" src="/vendor/drag-drop/excanvas.min.js"></script>

<script type="text/javascript" src="/vendor/exam/js/match.js"></script>
<script type="text/javascript" src="/vendor/exam/js/sequence.js"></script>
<script type="text/javascript" src="/vendor/exam/js/question.js"></script>
<script type="text/javascript" src="/vendor/exam/js/exam.js"></script>

<script type="text/javascript" src="/vendor/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: { inlineMath: [['$','$'],['\\(','\\)']] }
    });
</script>
<script>
    jQuery(document).ready(function () {
        App.init();
    });
</script>

</body>
</html>
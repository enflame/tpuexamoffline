<div class="exam_question_main">
    <form id="question-form"
          class="valid json form-horizontal" action="#" method="POST">


        <input type="hidden" name="key" value="5313b16168356f230b8dd30f61387c12">
        <input type="hidden" name="type" class="question_type" value="onechoice">

        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-6">

                <p>
                    <b>

                        Вопрос
                        №:&nbsp;
                        6745
                    </b>
                </p>

                <div class="question-content">

                    <p><strong>Уравнение2 для заданной магнитной цепи при напряженности <em>Н</em></strong></p>

                    <p><img alt="" src="/img/1ae7a313.png" /></p>

                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">

                <p>
                    <i>

                        Выберите один вариант:
                    </i>
                </p>

                <table class="table table-border table-no-border table-hover table-condensed">

                    <tr class="">



                        <td width="1%">
                            <nobr>
                                <label class="radio-success">
                                    <input class="styled choice"
                                           autocomplete="off"
                                           type="radio"
                                           id="input_15784"
                                           name="answer[6745]"
                                           value="15784"
                                    >
                                </label>
                            </nobr>
                        </td>
                        <td width="99%">
                            <label for="input_15784">
                                <span class="math-tex">\(\large B = - \frac{{{{\rm{\mu }}_0}l}}{{\rm{\delta }}}H\)</span>
                            </label>
                        </td>
                    </tr>
                    <tr class="">



                        <td width="1%">
                            <nobr>
                                <label class="radio-success">
                                    <input class="styled choice"
                                           autocomplete="off"
                                           type="radio"
                                           id="input_15781"
                                           name="answer[6745]"
                                           value="15781"
                                    >
                                </label>
                            </nobr>
                        </td>
                        <td width="99%">
                            <label for="input_15781">
                                <span class="math-tex">\(\)</span><span class="math-tex">\(\large {\rm{\Phi }} = - \frac{{{{\rm{\mu }}_0} \cdot l}}{{\rm{\delta }}}B\)</span>
                            </label>
                        </td>
                    </tr>
                    <tr class="">



                        <td width="1%">
                            <nobr>
                                <label class="radio-success">
                                    <input class="styled choice"
                                           autocomplete="off"
                                           type="radio"
                                           id="input_15782"
                                           name="answer[6745]"
                                           value="15782"
                                    >
                                </label>
                            </nobr>
                        </td>
                        <td width="99%">
                            <label for="input_15782">
                                <span class="math-tex">\(\large B = \frac{{{{\rm{\mu }}_0}l}}{{\rm{\delta }}}H\)</span>
                            </label>
                        </td>
                    </tr>
                    <tr class="">



                        <td width="1%">
                            <nobr>
                                <label class="radio-success">
                                    <input class="styled choice"
                                           autocomplete="off"
                                           type="radio"
                                           id="input_15783"
                                           name="answer[6745]"
                                           value="15783"
                                    >
                                </label>
                            </nobr>
                        </td>
                        <td width="99%">
                            <label for="input_15783">
                                <span class="math-tex">\(\large {\rm{\Phi }} = \frac{{{{\rm{\mu }}_0} \cdot l}}{{\rm{\delta }}}B\)</span>
                            </label>
                        </td>
                    </tr>
                </table>
                <div class="margin-top-small">


                </div>
            </div>
        </div>

    </form>
</div>
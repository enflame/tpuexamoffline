﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Threading;
using TPUExamLib;
using System.IO;

using System.Diagnostics;
namespace TPUExamOffline
{
  //
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
//    Settings _settings;
    Student _student;
    public MainWindow()
    {
  
  //    _settings = new Settings();
      try
      {
        _student = new Student();
        
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);

      }

      InitializeComponent();
    }

    private void b_Guest_Click(object sender, RoutedEventArgs e)
    {
  
      _student.Familiya = "Гость";

      wndExam wnd = new wndExam(_student);
      wnd.CloseHandler +=new wndExam.CloseEventHandler(wnd_CloseHandler);
      Hide();
      wnd.Show();

    }

    void wnd_CloseHandler(object sender)
    {
      Show();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      wndInvite wnd = new wndInvite(_student);
      wnd.CloseHandler += new wndInvite.CloseEventHandler(wnd_CloseHandler);
      Hide();
      wnd.Show();
    }

    void wnd_CloseHandler(object sender, wndInvite.CloseEventArgs args)
    {
      if (!args.IsReallyClose) Show();
      else
      {
        Close();
      }
    }
    void wnd_CloseHandler(object sender, wndSelectExam.CloseEventArgs args)
    {
      if (!args.IsReallyClose) Show();
      else
      {
        Close();
      }
    }

    private void b_close_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      TopPanel.HideClock(true);
    }
  }
}

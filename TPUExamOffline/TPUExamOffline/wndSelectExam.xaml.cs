﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using TPUExamLib;
namespace TPUExamOffline
{
  /// <summary>
  /// Логика взаимодействия для wndSelectExam.xaml
  /// </summary>
  public partial class wndSelectExam : Window
  {
    bool IsReallyClose = false;
    public wndSelectExam(Exam exam)
    {
      InitializeComponent();

      foreach (Package item in exam.packages)
      {

        SelectButton b = new SelectButton();
        b.Set(item, item.name);
        b.ButtonClick += new RoutedEventHandler(b_ButtonClick);
        wrapPanel.Children.Add(b);
      }

      //if (string.IsNullOrWhiteSpace(Exams.student.Familiya))
      //{
      //  b_prev.Content = "Вернуться в начало";
      //}
      //else
      //  b_prev.Content = "Вернуться к вводу личных данных";

      //tb_fio.Text = exam.student.FIO;

    }

    void b_ButtonClick(object sender, RoutedEventArgs e)
    {
      Exam item = (Exam)((SelectButton)sender).Tag;
      //wndSelectTest wnd = new wndSelectTest(item.tests);
      //wnd.CloseHandler += wnd_CloseHandler;

      //Hide();
      //wnd.Show();
    }
    void wnd_CloseHandler(object sender, wndSelectTest.CloseEventArgs args)
    {
      if (!args.IsReallyClose) Show();
      else 
      {
        IsReallyClose = true;
        Close();
        OnCloseEvent(new CloseEventArgs() { IsReallyClose = true });
      }
    }

    void LoadStyle()
    {
      //неудача
      ResourceDictionary res = (ResourceDictionary)Application.LoadComponent(new Uri("/TPUExamLib;component/Styles/Styles.xaml", UriKind.Relative));
      // b.Style = (Style)FindResource("b_style");
      //      Style o = Application.Current.FindResource("b_style") as Style;

      //     b.Style = (Style)Application.Current.Resources["b_style"];
      //     b.Style = (Style)res["b_style"];

      ResourceDictionary myresourcedictionary = new ResourceDictionary();
      myresourcedictionary.Source = new Uri("/TPUExamLib;component/Styles/Styles.xaml", UriKind.RelativeOrAbsolute);
      ResourceDictionary mystyles = new ResourceDictionary();
      mystyles.Source = new Uri("/TPUExamLib;component/Styles/Styles.xaml", UriKind.RelativeOrAbsolute);
      Style mybuttonstyle = mystyles["b_style"] as Style;
    }

    private void b_prev_Click(object sender, RoutedEventArgs e)
    {
      IsReallyClose = true;
      Close();
      OnCloseEvent(new CloseEventArgs() { IsReallyClose=false});
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (!IsReallyClose) e.Cancel = true;
    }

    #region events
    public class CloseEventArgs : EventArgs
    {
      public bool IsReallyClose;
    }

    public delegate void CloseEventHandler(Object sender, CloseEventArgs args);
    public event CloseEventHandler CloseHandler;
    protected virtual void OnCloseEvent(CloseEventArgs args)
    {
      if (CloseHandler != null)
      {
        CloseHandler(this, args);
      }
    }

    #endregion events

  }
}

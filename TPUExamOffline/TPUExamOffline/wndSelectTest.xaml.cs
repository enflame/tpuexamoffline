﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using TPUExamLib;
namespace TPUExamOffline
{
  /// <summary>
  /// Логика взаимодействия для wndSelectTest.xaml
  /// </summary>
  public partial class wndSelectTest : Window
  {
    bool IsReallyClose = false;
    public wndSelectTest()
    {
      InitializeComponent();

      //foreach (Test item in tests)
      //{
      //  SelectButton b = new SelectButton();
      //  b.Set(item, item.name);
      //  b.ButtonClick += new RoutedEventHandler(b_ButtonClick);
      //  wrapPanel.Children.Add(b);
      //}
      //tb_fio.Text = Exams.student.FIO;

    }
    void b_ButtonClick(object sender, RoutedEventArgs e)
    {
      //Test item = (Test)((SelectButton)sender).Tag;
      //wndExam wnd = new wndExam(item);
      //wnd.Show();
      //IsReallyClose = true;
      //Close();
      //OnCloseEvent(new CloseEventArgs() { IsReallyClose = true });
    }

    private void b_prev_Click(object sender, RoutedEventArgs e)
    {
      IsReallyClose = true;
      Close();
      OnCloseEvent(new CloseEventArgs() { IsReallyClose = false });
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (!IsReallyClose) e.Cancel = true;
    }


    #region events
    public class CloseEventArgs : EventArgs
    {
      public bool IsReallyClose;
    }

    public delegate void CloseEventHandler(Object sender, CloseEventArgs args);
    public event CloseEventHandler CloseHandler;
    protected virtual void OnCloseEvent(CloseEventArgs args)
    {
      if (CloseHandler != null)
      {
        CloseHandler(this, args);
      }
    }

    #endregion events

  }
}

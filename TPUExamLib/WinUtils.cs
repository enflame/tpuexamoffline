﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Diagnostics;
namespace TPUExamLib
{
  public class WinUtils
  {
    //
    [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern IntPtr LoadLibrary(string lpFileName);

    [DllImport("user32", EntryPoint = "CloseWindow111", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    static extern bool CloseWindow(int hWnd);
    [DllImport("user32", EntryPoint = "GetWindowText", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
    [DllImport("user32", EntryPoint = "GetWindowTextLength", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    static extern int GetWindowTextLength(IntPtr hWnd);

    [DllImport("user32", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc callback, IntPtr hInstance, uint threadId);
    [DllImport("user32", EntryPoint = "UnhookWindowsHookEx", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    static extern bool UnhookWindowsHookEx(IntPtr hInstance);
    [DllImport("user32.dll")]
    static extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, int wParam, IntPtr lParam);

 

    const int WM_KEYDOWN = 0x100; // Сообщения нажатия клавиши
    const int WM_KEYDOWN_ALT = 0x104; // Сообщения нажатия клавиши
    static int vkCodePrev = 0; //код предыдущей клавиши
    static List<int> vkCodes = new List<int>();
    
    const int WH_KEYBOARD_LL = 13; // Номер глобального LowLevel-хука на клавиатуру
    private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
    private LowLevelKeyboardProc _proc = hookProc;
    private static IntPtr hhook = IntPtr.Zero;

    const int WH_SHELL = 10; // Номер глобального LowLevel-хука 
    private delegate IntPtr LowLevelWindowProc(int nCode, IntPtr wParam, IntPtr lParam);
    private LowLevelKeyboardProc _winproc = hookWinProc;
   // private static IntPtr hhook = IntPtr.Zero;



    /*code needed to disable start menu*/
    [DllImport("user32.dll")]
    private static extern int FindWindow(string className, string windowText);
    [DllImport("user32.dll")]
    private static extern int ShowWindow(int hwnd, int command);
    private const int SW_HIDE = 0;
    private const int SW_SHOW = 1;



    public void HideStartMenu()
    {
      int hwnd = FindWindow("Shell_TrayWnd", "");
      ShowWindow(hwnd, SW_HIDE);
      }
    public void ShowStartMenu()
    {
      int hwnd = FindWindow("Shell_TrayWnd", "");
      ShowWindow(hwnd, SW_SHOW);
    }
    public void DisableTaskManager()
    {
      RegistryKey regkey;
      string keyValueInt = "1";
      string subKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System";

      regkey = Registry.CurrentUser.CreateSubKey(subKey);
      regkey.SetValue("DisableTaskMgr", keyValueInt);
      regkey.Close();
    }
    public void EnableTaskManager()
    {
      string subKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System";
      RegistryKey rk = Registry.CurrentUser;
      RegistryKey sk1 = rk.OpenSubKey(subKey);
      if (sk1 != null)
        rk.DeleteSubKeyTree(subKey);
    }

    static IntPtr hookWinProc(int code, IntPtr wParam, IntPtr lParam)
    {
      int textLength = GetWindowTextLength(lParam);
      StringBuilder outText = new StringBuilder(textLength + 1);
      int a = GetWindowText(lParam, outText, outText.Capacity);


      textLength = GetWindowTextLength(wParam);
      outText = new StringBuilder(textLength+1);
       a = GetWindowText(wParam, outText, outText.Capacity);
      
      //vkCodes.Add(Marshal.ReadInt32(lParam));
      //CloseWindow((int)wParam);
      ShowWindow((int)wParam, SW_HIDE);
    //  return (IntPtr)1;
      return CallNextHookEx(hhook, code, (int)wParam, lParam);
    }
    public void StartLockWindow()
    {
      IntPtr hInstance = LoadLibrary("User32");
      hhook = SetWindowsHookEx(WH_SHELL, _winproc, hInstance, 0);
    }

    static IntPtr hookProc(int code, IntPtr wParam, IntPtr lParam)
    {
 //     vkCodes.Add(Marshal.ReadInt32(lParam));
      if (code >= 0 && wParam == (IntPtr)WM_KEYDOWN_ALT)
      {
        int vkCode = Marshal.ReadInt32(lParam);
        vkCodePrev = vkCode;
        return (IntPtr)1;
      }

      if (code >= 0 && wParam == (IntPtr)WM_KEYDOWN)
      {
        int vkCode = Marshal.ReadInt32(lParam);
  
         
        if (vkCode.ToString() == "91") return (IntPtr)1;//win
        if (vkCode.ToString() == "27") return (IntPtr)1;//esc
        if (vkCode.ToString() == "9") return (IntPtr)1;//tab
        if (vkCode.ToString() == "162") return (IntPtr)1;//ctrl
        if (vkCode.ToString() == "46") return (IntPtr)1;//del
        if (vkCode.ToString() == "13") return (IntPtr)1;//enter
        //     if ((vkCode.ToString() == "162") && (vkCodePrev.ToString() == "162")) return (IntPtr)1;//cntr + alt


        vkCodePrev = vkCode;
      }

      return CallNextHookEx(hhook, code, (int)wParam, lParam);
    }

    public void StartLockKeys()
    {
      IntPtr hInstance = LoadLibrary("User32");
      hhook = SetWindowsHookEx(WH_KEYBOARD_LL, _proc, hInstance, 0);

    }
    public void EndLockKeys()
    {
      UnhookWindowsHookEx(hhook);
    }

  }
}

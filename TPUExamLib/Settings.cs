﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

namespace TPUExamLib
{
  public class Settings
  {
    public static readonly string CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

    public static string PathToTemplate { get { return Settings.CurrentDirectory + "\\Vendor\\template\\content.tpl"; } }


    public static string PathToExamen { get { return Settings.CurrentDirectory + "\\Examen"; } }
    public static string PathToExamenXml { get { return PathToExamen + "\\examen.xml"; } }

  }

  public class Exam
  {
    public string id;
    public string name = "";
    public List<Package> packages;

    public Exam()
    {
      packages = new List<Package>();
      Load();
    }

    void Load()
    {
      try
      {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(Settings.PathToExamenXml);
        XmlNode xRoot = xmlDoc.DocumentElement;

        foreach (XmlNode xexam in xRoot)
        {
          if (xexam.Name == "title")
          {
            name = xexam.InnerText;
          }
          if (xexam.Name == "module")
          {
            foreach (XmlNode xpack in xexam)
            {
              Package pack = new Package();
              pack.name = xpack.InnerText;

              XmlNode attr = xpack.Attributes.GetNamedItem("id");
              if (attr != null) pack.id = Convert.ToInt32(attr.Value);

              attr = xpack.Attributes.GetNamedItem("ball");
              if (attr != null) pack.ball = Convert.ToInt32(attr.Value);

              pack.Load();
              packages.Add(pack);
            }
          }
        }
      }
      catch (Exception exc)
      {
        throw new Exception("Exam.Load: " + exc.Message);
      }

    }

  }
  public class Package
  {
    public int id;
    public string name;
    public string Path
    {
      get
      {
        return Settings.PathToExamen + "\\package_" + id;
      }
    }

    public int ball;

    public Ticket ticket;//выбирается случайным образом
    Random rnd;

    public Package()
    {
      rnd = new Random();
      ticket = new Ticket();
    }
    public void Load()
    {
      try
      {
        string[] ticket_dirs = System.IO.Directory.GetDirectories(Path + "\\");

        string ticket_dir = ticket_dirs[rnd.Next(ticket_dirs.Length - 1)];

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(ticket_dir + "\\ticket.xml");
        XmlNode xRoot = xmlDoc.DocumentElement;

        foreach (XmlNode xticket in xRoot)
        {
          if (xticket.Name == "title")
          {
            ticket.name = xticket.InnerText;
          }
          if (xticket.Name == "content")
          {
            foreach (XmlNode xquest in xticket)
            {
              Question qst = new Question();
              qst.name = xquest.InnerText;

              XmlNode attr = xquest.Attributes.GetNamedItem("id");
              if (attr != null) qst.id = Convert.ToInt32(attr.Value);

              attr = xquest.Attributes.GetNamedItem("type");
              if (attr != null) qst.type = (attr.Value);

              qst.Load(ticket_dir);

              ticket.AddQuestion(qst);
            }
          }
        }


      }
      catch (Exception exc)
      {
        throw new Exception("Package.Load: " + exc.Message);
      }

    }

    public int GetCountAnswered()
    {
      int res = 0;


      return res;
    }
  }
  public class Ticket
  {
    public int id;
    public string name;
    List<Question> _questions;

    int AnsweredCount;

    public Ticket()
    {
      _questions = new List<Question>();
    }

    public int GetCountAnswered()
    {
      int res = 0;
      foreach (Question q in _questions)
        if (q.IsAnswered) res++;

      return res;
    }
    public void AddQuestion(Question q)
    {
  //    q.SetAnswerHandler += new Question.SetAnswerEventHandler(q_SetAnswerHandler);
      _questions.Add(q);
    }
    public List<Question> GetQuestions()
    {
      return _questions;
    }
    void q_SetAnswerHandler(object sender)
    {
      throw new NotImplementedException();
    }
  }

  public class Question
  {
    public int id;
    public string name;
    public string type;
    public string Dir;
    public string PathToHtml { get { return Dir + "\\Question.html"; } }

   public  bool IsAnswered { get { if (Answer != null)return true; else return false; } }

   object Answer;

    public void Load(String PathToTicket)
    {
      try
      {
        Dir = PathToTicket + "\\position_" + id;
        System.IO.File.Delete(PathToHtml);

        string qst = System.IO.File.ReadAllText(Dir + "\\content.tpl");
        string tpl = System.IO.File.ReadAllText(Settings.PathToTemplate);

        tpl = tpl.Replace("/vendor", "../../../../vendor");
        tpl = tpl.Replace("{{BODY}}", qst);
        System.IO.File.WriteAllText(PathToHtml, tpl);

      }
      catch (Exception exc)
      {
        throw new Exception("Package.Load: " + exc.Message);
      }

    }

    public void SetAnswer(object Answer)
    {
      this.Answer = Answer;
      //  OnSetAnswerEvent();
    }


    //#region events
    //public delegate void SetAnswerEventHandler(Object sender);
    //public event SetAnswerEventHandler SetAnswerHandler;
    //protected virtual void OnSetAnswerEvent()
    //{
    //  if (SetAnswerHandler != null)
    //  {
    //    SetAnswerHandler(this);
    //  }
    //}

    //#endregion events

  }

  [Serializable]
  public class Student
  {
    public Exam Examen;

    public string Familiya { get; set; }
    public string Imya { get; set; }
    public string Otchestvo { get; set; }
    public string FIO
    {
      get
      {
        if (string.IsNullOrWhiteSpace(Familiya)) return "Гостевой доступ";
        return Familiya + " " + Imya + " " + Otchestvo;
      }

    }
    //
    public string Document { get; set; }
    public string DocData { get; set; }
    public string Address { get; set; }

    public Student()
    {
      Examen = new Exam();
    }
  }
}

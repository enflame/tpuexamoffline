﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TPUExamLib
{
  /// <summary>
  /// Логика взаимодействия для QPaginatorPackage.xaml
  /// </summary>
  public partial class QPaginatorPackage : UserControl
  {
    Package _package;
    Brush background = Brushes.DarkCyan;

    bool IsSelected;
    public QPaginatorPackage()
    {
      InitializeComponent();

      Set(false);
    }

    public void Set(Package package)
    {
      _package = package;
      tb_content.Text = package.name;
    }

    public void Set(bool IsSelected)
    {
      if (IsSelected)
      {
        grid.Background = background;
        border.Background = background;

        tb_content.Foreground = Brushes.White;
        

        tb_info.Background = Brushes.Red;
        tb_info.Foreground = Brushes.White;

      }
      else 
      {
        grid.Background = Brushes.White;
        border.Background = Brushes.White;

        tb_content.Foreground = Brushes.Black;

        tb_info.Background = background;
        tb_info.Foreground = Brushes.Black;

      }
      this.IsSelected = IsSelected;
    }
    public void UpdateInfo()
    {
      try
      {
        int count = 0;

        foreach (Question q in _package.ticket.GetQuestions())
        {
          if (q.IsAnswered) count++;
        }

        tb_info.Text = count + "/" + _package.ticket.GetQuestions().Count;
      }
      catch { }
    }

    private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (!IsSelected)
      {
        Set(true);
        OnClickEvent(null);
      }
    }

  
    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {

    }

    #region events
    public class ClickEventArgs : EventArgs
    {
    }

    public delegate void ClickEventHandler(Object sender, ClickEventArgs args);
    public event ClickEventHandler ClickHandler;
    protected virtual void OnClickEvent(ClickEventArgs args)
    {
      if (ClickHandler != null)
      {
        ClickHandler(this, args);
      }
    }

    #endregion events

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TPUExamLib
{
  /// <summary>
  /// Логика взаимодействия для QPaginatorPrevNext.xaml
  /// </summary>
  public partial class QPaginatorPrevNext : UserControl
  {
    bool IsLeft;
    public QPaginatorPrevNext()
    {
      InitializeComponent();
    }

    public void Set(bool IsLeft) 
    {
      if (IsLeft)
      {
        path_LR.Visibility = System.Windows.Visibility.Collapsed;
        path_RL.Visibility = System.Windows.Visibility.Visible;
        tb_content.Text = "Предыдущее задание";
      }
      else 
      {
        path_LR.Visibility = System.Windows.Visibility.Visible;
        path_RL.Visibility = System.Windows.Visibility.Collapsed;
        tb_content.Text = "Следующее задание";
      }
      this.IsLeft = IsLeft;
      border_MouseLeave(null, null);
    }

    private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      OnClickEvent(new ClickEventArgs() { IsLeft = this.IsLeft });
    }


    #region events
    public class ClickEventArgs : EventArgs
    {
      public bool IsLeft;
    }

    public delegate void ClickEventHandler(Object sender, ClickEventArgs args);
    public event ClickEventHandler ClickHandler;
    protected virtual void OnClickEvent(ClickEventArgs args)
    {
      if (ClickHandler != null)
      {
        ClickHandler(this, args);
      }
    }

    #endregion events

    private void border_MouseMove(object sender, MouseEventArgs e)
    {
      border.Background = Brushes.DarkSlateGray;
      path_LR.Stroke = Brushes.White;
      path_RL.Stroke = Brushes.White;
      tb_content.Foreground = Brushes.White;
    }

    private void border_MouseLeave(object sender, MouseEventArgs e)
    {
      border.Background = Brushes.White;
      path_LR.Stroke = Brushes.Black;
      path_RL.Stroke = Brushes.Black;
      tb_content.Foreground = Brushes.Black;
    }

  }
}

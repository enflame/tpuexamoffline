﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TPUExamLib
{
  /// <summary>
  /// Логика взаимодействия для QPaginator.xaml
  /// </summary>
  public partial class QPaginator : UserControl
  {
    public string Caption { get { return (string) button.Content; } set { button.Content = value; } }
    public Question _question;

    bool IsPressed;
    public QPaginator QPaginatorPrev;
    public QPaginator QPaginatorNext;

    public QPaginator()
    {
      InitializeComponent();
      Set(false);
    }

    public void Set(Question question)
    {
      _question = question;
      Caption = question.id+"";
     }

    public void Set(bool IsPressed)
    {
      if (IsPressed)
      {
        button.BorderBrush = Brushes.DarkGreen;
        button.Background = Brushes.LightGray;
        button.Foreground = Brushes.Green;
      }
      else 
      {
        button.BorderBrush = Brushes.Gray;
        button.Background = Brushes.White;
        button.Foreground = Brushes.Black;
      }
      this.IsPressed = IsPressed;
    }

    private void button_Click(object sender, RoutedEventArgs e)
    {
      if (!IsPressed)
      {
        OnClickEvent(new ClickEventArgs());
      }
    }

    #region events
    public class ClickEventArgs : EventArgs
    {
    }

    public delegate void ClickEventHandler(Object sender, ClickEventArgs args);
    public event ClickEventHandler ClickHandler;
    protected virtual void OnClickEvent(ClickEventArgs args)
    {
      if (ClickHandler != null)
      {
        ClickHandler(this, args);
      }
    }

    #endregion events

  }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Threading;
namespace TPUExamLib
{
  /// <summary>
  /// Логика взаимодействия для SelectButton.xaml
  /// </summary>
  public partial class SelectButton : UserControl
  {
    private DispatcherTimer _dayTimer;

    public SelectButton()
    {
      InitializeComponent();
      this.Loaded += new RoutedEventHandler(UserControl_Loaded);
    }
    public void Set(object obj, string txt)
    {
      this.Tag = obj;

      //tb_b_content.Text = txt;
    }

    public event RoutedEventHandler ButtonClick;

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      if (ButtonClick != null) ButtonClick(this, e);
    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
      // set the datacontext to be today's date
      DateTime now = DateTime.Now;
      DataContext = now.Day.ToString();

      // then set up a timer to fire at the start of tomorrow, so that we can update
      // the datacontext
      _dayTimer = new DispatcherTimer();
      _dayTimer.Interval = new TimeSpan(1, 0, 0, 0) - now.TimeOfDay;
      _dayTimer.Tick += new EventHandler(OnDayChange);
      _dayTimer.Start();

      // finally, seek the timeline, which assumes a beginning at midnight, to the appropriate
      // offset
      Storyboard sb = (Storyboard)PodClock.FindResource("sb");
      sb.Begin(PodClock, HandoffBehavior.SnapshotAndReplace, true);
      sb.Seek(PodClock, now.TimeOfDay, TimeSeekOrigin.BeginTime);
    }

    private void OnDayChange(object sender, EventArgs e)
    {
      // date has changed, update the datacontext to reflect today's date
      DateTime now = DateTime.Now;
      DataContext = now.Day.ToString();
      _dayTimer.Interval = new TimeSpan(1, 0, 0, 0);
    }
  }



}

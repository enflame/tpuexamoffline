﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
namespace TPUExamLib
{
  public class ExamHttpServer
  {
    HttpListener listener;

    volatile bool _isWorking;
    Thread th_Listen;
    Settings settings = new Settings();
    public void Start()
    {
      string url = "http://127.0.0.1";
      string port = "9999";
      string prefix = String.Format("{0}:{1}/", url, port);

      listener = new HttpListener();
      listener.Prefixes.Add(prefix);

      listener.Start();
      _isWorking = true;
      th_Listen = new Thread(Listen);
      th_Listen.Start();


    }
    string GetTicket()
    {
      return "";// File.ReadAllText(Settings.PathToTickets + @"\Вопрос №  6706 _ Оценка результатов и компетенций.htm");
    }
    void Listen()
    {
      while (_isWorking)
      {
        try
        {
          //Ожидание входящего запроса
          HttpListenerContext context = listener.GetContext();
          //Объект запроса
          HttpListenerRequest request = context.Request;
          //Объект ответа
          HttpListenerResponse response = context.Response;
          //Создаем ответ
          string responseString = GetTicket();

          var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
          response.ContentLength64 = buffer.Length;
          var output = response.OutputStream;
          output.Write(buffer, 0, buffer.Length);
          output.Close();


          //string requestBody;
          //Stream inputStream = request.InputStream;
          //Encoding encoding = request.ContentEncoding;
          //StreamReader reader = new StreamReader(inputStream, encoding);
          //requestBody = reader.ReadToEnd();


          ExamHttpServerEventarg arg = new ExamHttpServerEventarg();
          OnExamHttpServerEvent(arg);

          response.StatusCode = (int)HttpStatusCode.OK;

          //Возвращаем ответ
          using (Stream stream = response.OutputStream) { }
        }
        catch (Exception exc)
        {
          //  throw exc;
        }
      }
    }
    public void Stop()
    {
      _isWorking = false;
      try
      {
        listener.Stop();
      }
      catch
      {
      }
    }

    #region Messages
    public class ExamHttpServerEventarg : EventArgs
    {

    }
    public delegate void ExamHttpServerEventHandler(ExamHttpServerEventarg mess);
    public event ExamHttpServerEventHandler ExamHttpServerEvent;
    protected virtual void OnExamHttpServerEvent(ExamHttpServerEventarg arg)
    {
      if (ExamHttpServerEvent != null)
      {
        ExamHttpServerEvent(arg);
      }
    }
    #endregion

  }
  //  class ExamHttpServer: HttpServer {
  //    public ExamHttpServer(int port)
  //            : base(port) { }


  //        public override void handleGETRequest(HttpProcessor p) {
  //            Console.WriteLine("request: {0}", p.http_url);
  //            p.writeSuccess();
  //            p.outputStream.WriteLine("<html><body><h1>test server</h1>");
  //            p.outputStream.WriteLine("Current Time: " + DateTime.Now.ToString());
  //            p.outputStream.WriteLine("url : {0}", p.http_url);

  //            p.outputStream.WriteLine("<form method=post action=/form>");
  //            p.outputStream.WriteLine("<input type=text name=foo value=foovalue>");
  //            p.outputStream.WriteLine("<input type=submit name=bar value=barvalue>");
  //            p.outputStream.WriteLine("</form>");
  //        }

  //        public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData) {
  //            Console.WriteLine("POST request: {0}", p.http_url);
  //            string data = inputData.ReadToEnd();

  //            p.outputStream.WriteLine("<html><body><h1>test server</h1>");
  //            p.outputStream.WriteLine("<a href=/test>return</a><p>");
  //            p.outputStream.WriteLine("postbody: <pre>{0}</pre>", data);


  //        }
  //    }

  //  public abstract class HttpServer {

  //    protected int port;
  //    TcpListener listener;
  //    bool is_active = true;

  //    public HttpServer(int port) {
  //        this.port = port;
  //    }

  //    public void listen() {
  //        listener = new TcpListener(port);
  //        listener.Start();
  //        while (is_active) {                
  //            TcpClient s = listener.AcceptTcpClient();
  //            HttpProcessor processor = new HttpProcessor(s, this);
  //            Thread thread = new Thread(new ThreadStart(processor.process));
  //            thread.Start();
  //            Thread.Sleep(1);
  //        }
  //    }

  //    public abstract void handleGETRequest(HttpProcessor p);
  //    public abstract void handlePOSTRequest(HttpProcessor p, StreamReader inputData);
  //} 
}

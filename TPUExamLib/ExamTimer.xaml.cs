﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TPUExamLib
{
    /// <summary>
    /// Логика взаимодействия для ExamTimer.xaml
    /// </summary>
    public partial class ExamTimer : UserControl
    {
        DispatcherTimer _dayTimer;
        TimeSpan _ts;
        TimeSpan _dts;
        public ExamTimer()
        {
            InitializeComponent();

         }
    public void Set(TimeSpan ts)
    {
      InitializeComponent();

      _ts = ts;
      _dts = new TimeSpan(0, 1, 0);
    }
    public void Start()
        {
            _dayTimer = new DispatcherTimer();
            _dayTimer.Interval = new TimeSpan(0, 0, 1, 0);
            _dayTimer.Tick += new EventHandler(OnMinChange);
            _dayTimer.Start();
        }
        private void OnMinChange(object sender, EventArgs e)
        {
            _ts = _ts - _dts;
            TText.Text = _ts.ToString(); 
           }
    }
}
